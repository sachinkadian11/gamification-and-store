public class RedemptionPoints {
    /*
     * Method Name : getRewardItems
     * Created By  : Raju
     * Created date: 02/28/2019
     * Description : This method is used to update the contact Current_Points__c field based on the rewardtype id we sent
     *                and return success or error messages.
     */
    @AuraEnabled(cacheable = true)
    public static RewardWrapper getRewardItems() {
        RewardWrapper result = new RewardWrapper();

        Id userId = UserInfo.getUserId();
        String errormessage = '';

        //get current Contact
        Contact contactRecord = [SELECT Id, Total_Points__c, Employee__c FROM Contact WHERE Employee__c =: userId LIMIT 1];

        result.storeItems = new List < ItemWrapper > ();
        for (Store_Item__c item: [select id, name, Description__c, price__c, Image_Id__c, Quantity_Available__c from Store_Item__c]) {
            result.storeItems.add(new ItemWrapper(item));
        }

        result.purchaseHistory = new List < ItemWrapper > ();
        for (Employee_Redemption__c item: [select id, Name, Store_Item__r.Name, Points_Redeemed__c, createdDate, status__c from Employee_Redemption__c where Employee__c =: contactRecord.Id]) {
            result.purchaseHistory.add(new ItemWrapper(item));
        }

        result.currentPoints = contactRecord.Total_Points__c;

        return result;
    }

    @AuraEnabled
    public static void buyItem(String itemId) {
        Employee_Redemption__c redemption = new Employee_Redemption__c();
        Contact contactRecord = [SELECT Id, Total_Points__c, Employee__c FROM Contact WHERE Employee__c =: UserInfo.getUserId() LIMIT 1];
        Store_Item__c item = [select id, Price__c from Store_Item__c where id =: itemId];

        if (item.Price__c > contactRecord.Total_Points__c) {
            throw new AuraHandledException('You do not have enough points to buy this item');
        } else {
            redemption.Employee__c = contactRecord.Id;
            redemption.Store_Item__c = itemId;
            redemption.Status__c = 'Under Approval';
            redemption.Points_Redeemed__c = item.Price__c;
            try {
                insert redemption;

                //submit record to approval process for HR/Finance approval
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for HR/Finance approval');
                approvalRequest.setObjectId(redemption.id);
                approvalRequest.setSubmitterId(UserInfo.getUserId());
                approvalRequest.setProcessDefinitionNameOrId('Reward_Point_Redemption_Approval');
                Approval.process(approvalRequest);

            } catch (Exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }
    }

    public class RewardWrapper {
        @AuraEnabled public List < ItemWrapper > storeItems;
        @AuraEnabled public List < ItemWrapper > purchaseHistory;
        @AuraEnabled public Decimal currentPoints;
    }

    public class ItemWrapper {
        @AuraEnabled public Store_Item__c item;
        @AuraEnabled public String itemName;
        @AuraEnabled public Decimal itemPrice;
        @AuraEnabled public String itemDescription;
        @AuraEnabled public String imageURL;
        @AuraEnabled public String itemId;
        @AuraEnabled public DateTime purchaseDate;
        @AuraEnabled public String itemStatus;
        @AuraEnabled public String cssClasses;

        public ItemWrapper(Store_Item__c item) {
            this.item = item;
            this.itemName = item.Name;
            this.itemPrice = item.Price__c;
            this.itemDescription = item.Description__c;
            this.itemId = item.Id;
            this.imageURL = '/sfc/servlet.shepherd/version/download/' + item.Image_Id__c;
        }

        public ItemWrapper(Employee_Redemption__c item) {
            this.itemName = item.Store_Item__r.Name;
            this.itemPrice = item.Points_Redeemed__c;
            this.purchaseDate = item.createdDate;
            this.itemStatus = item.Status__c;
            this.cssClasses = 'card card-history slds-m-bottom_small slds-p-around_small text-white';
            if (item.status__c == 'Under Approval') {
                this.cssClasses += ' card-under-approval';
            } else if (item.status__c == 'Approved') {
                this.cssClasses += ' card-approved';
            } else if (item.status__c == 'Rejected') {
                this.cssClasses += ' card-rejected';
            }
        }
    }
}