import { LightningElement, track,wire } from 'lwc';
import getStoreItem from '@salesforce/apex/RedemptionPoints.getRewardItems';
import buyItem from '@salesforce/apex/RedemptionPoints.buyItem';
import { reduceErrors } from 'c/util';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class market_home extends LightningElement {
    @track userPoint = 0;
    @track listOfProducts ;
    @track minPrice = 0;
    @track maxPrice = 100;
    allProducts;
    @track currentItemName;
    @track currentItemPrice;
    @track currentItemId;
    @track purchaseHistoryList;
    @wire(getStoreItem)
    wiredItems({error,data}){
        if(data){
            this.userPoint = data.currentPoints;
            this.listOfProducts = data.storeItems;
            this.purchaseHistoryList = JSON.parse(JSON.stringify(data.purchaseHistory));
            let allPriceArray = data.storeItems.map(product => product.itemPrice);
            this.allProducts = data.storeItems;
            this.minPrice = Math.min(...allPriceArray);
            this.maxPrice = Math.max(...allPriceArray);
        } else if(error){
            console.log('error');
        }
    }

    handleClick(event){
        this.currentItemId = event.target.getAttribute('data-id');
        this.currentItemName = event.target.getAttribute('data-name');
        this.currentItemPrice = event.target.getAttribute('data-price');
        this.openModal();
    }

    filterProducts(event){
        let priceFilter = event.target.value;
        this.listOfProducts = this.allProducts.filter(product => product.itemPrice <= priceFilter);
    }

    confirmPurchase(event){
        let buyItemSuccess = result => {
            this.purchaseHistoryList.push({
                itemName : this.currentItemName,
                itemPrice : this.currentItemPrice,
                purchaseDate : new Date(),
                itemStatus : 'Under Approval',
                cssClasses : 'card card-history slds-m-bottom_small slds-p-around_small text-white card-under-approval'
            });
            this.userPoint = this.userPoint - this.currentItemPrice;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Thank You',
                    message: 'Your request has been submitted to Finance/HR department',
                    variant: 'success'
                })
            );
            this.closeModal();
        }

        let buyItemError = error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: reduceErrors(error).join(', '),
                    variant: 'error'
                })
            );
            this.closeModal();
        }

        buyItem({itemId : this.currentItemId}).then(buyItemSuccess).catch(buyItemError);
    }

    openModal(){
        this.template.querySelector('.purchage-confirmation-modal').classList.add('slds-fade-in-open');
        this.template.querySelector('.slds-backdrop').classList.add('slds-backdrop_open');
    }

    closeModal(){
        this.template.querySelector('.purchage-confirmation-modal').classList.remove('slds-fade-in-open');
        this.template.querySelector('.slds-backdrop').classList.remove('slds-backdrop_open');
    }
}