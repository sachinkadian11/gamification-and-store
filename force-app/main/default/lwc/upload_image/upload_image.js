import { LightningElement, track,wire,api } from 'lwc';
import changeImageId from '@salesforce/apex/UploadImageController.uploadImage';
import { getRecord } from 'lightning/uiRecordApi';
import IMAGE_ID from '@salesforce/schema/Store_Item__c.Image_Id__c';

export default class upload_image extends LightningElement {
    @api recordId;
    @track imageURL;
    get acceptedFormats() {
        return ['.jpg', '.png'];
    }

    //get records current image
    @wire(getRecord, { recordId: '$recordId', fields: [IMAGE_ID] })
    wiredImageId(result){
        this.wiredImageHistory = result;
        console.log(JSON.stringify(result.data));
        if(result.data ){
            this.imageURL = '/sfc/servlet.shepherd/version/download/'+result.data.fields.Image_Id__c.value;
        }else if(result.error){
            console.log(error);
        }
    }

    handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files[0];

        //update current record with the image id
       console.log(JSON.stringify(uploadedFiles));

        let onImageIdChangeSuccess = result => {
            refreshApex(this.wiredImageHistory);
        }
        let onImageIdChangeError = error => {
            
        }
        changeImageId({contentDocumentId : uploadedFiles.documentId , recordId : this.recordId}).then(onImageIdChangeSuccess).catch(onImageIdChangeError);
    }
}